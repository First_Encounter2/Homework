"""
Домашнее задание №1
Функции и структуры данных
"""


def power_numbers(*numbers, power=2):
    """
    функция, которая принимает N целых чисел,
    и возвращает список квадратов этих чисел
    >>> power_numbers(1, 2, 5, 7)
    <<< [1, 4, 25, 49]
    """
    return [number ** power for number in numbers]


# filter types
ODD = "odd"
EVEN = "even"
PRIME = "prime"


def is_odd(number):
    return number % 2 != 0


def is_even(number):
    return number % 2 == 0


def is_prime(number):
    if number > 1:
        for i in range(2, number):  # в действительности не обязательно проверять до `number`, достаточно до `number / 2`, а если еще строже, то до квадратного корня от `number`
            if number % i == 0:
                return False
    else:
        return False
    return True


def filter_numbers(list_of_numbers, filter_word):
    """
    функция, которая на вход принимает список из целых чисел,
    и возвращает только чётные/нечётные/простые числа
    (выбор производится передачей дополнительного аргумента)

    >>> filter_numbers([1, 2, 3], ODD)
    <<< [1, 3]
    >>> filter_numbers([2, 3, 4, 5], EVEN)
    <<< [2, 4]
    """
    # Чтобы не плодить `if` можно было сделать `dict` с соответствием `filter_word` -> function и брать функцию оттуда, а если ее нет в словаре, значит она не поддерживается
    if filter_word == ODD:
        return list(filter(is_odd, list_of_numbers))

    if filter_word == EVEN:
        return list(filter(is_even, list_of_numbers))

    if filter_word == PRIME:
        return list(filter(is_prime, list_of_numbers))
    # А если передан неподдерживаемый `filter_word`? По-хорошему, стоит выкидывать исключение, но раз их еще в лекциях не было, то хотя бы возвращать пустой список
